package dev.keii.ssisauth.chunks;

import dev.keii.ssisauth.SSISAuth;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ChunksDatabase {
    public static Connection getConnection() {
        try {
            final String url = SSISAuth.config.chunksDbUrl + SSISAuth.config.chunksDbName;

            return DriverManager.getConnection(url, SSISAuth.config.chunksDbUser, SSISAuth.config.chunksDbPassword);
        } catch (SQLException e) {
            Bukkit.getServer().sendMessage(Component.text("Fatal Database Error: " + e.getMessage()).color(NamedTextColor.RED));
            return null;
        }
    }
}