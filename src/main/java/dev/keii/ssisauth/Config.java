package dev.keii.ssisauth;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public String apiUrl;

    public String chunksDbUrl;
    public String chunksDbName;
    public String chunksDbUser;
    public String chunksDbPassword;


    public void fileConfigToConfig(FileConfiguration config)
    {
        chunksDbUrl = config.getString("chunksDbUrl");
        chunksDbName = config.getString("chunksDbName");
        chunksDbUser = config.getString("chunksDbUser");
        chunksDbPassword = config.getString("chunksDbPassword");
        apiUrl = config.getString("apiUrl");
    }

    public void loadConfig() {
        FileConfiguration config = SSISAuth.getInstance().getConfig();

        if(config.getString("chunksDbUrl") == null)
            config.set("chunksDbUrl", "jdbc:mysql://localhost:3306/");
        if(config.getString("chunksDbName") == null)
            config.set("chunksDbName", "");
        if(config.getString("chunksDbUser") == null)
            config.set("chunksDbUser", "");
        if(config.getString("chunksDbPassword") == null)
            config.set("chunksDbPassword", "");
        if(config.getString("apiUrl") == null)
            config.set("apiUrl", "");

        config.options().copyDefaults(true);
        SSISAuth.getInstance().saveConfig();

        config = SSISAuth.getInstance().getConfig();

        if(config.getString("chunksDbName").isEmpty())
        {
            Bukkit.broadcast(Component.text("Database is not set in the config. Core functionality of the plugin ").color(NamedTextColor.RED)
                    .append(
                            Component.text("will not")
                                    .decorate(TextDecoration.ITALIC)
                                    .decorate(TextDecoration.BOLD))
                    .color(NamedTextColor.RED)
                    .append(
                            Component.text(" work")
                                    .color(NamedTextColor.RED)));
        }

        fileConfigToConfig(config);
    }
}
