package dev.keii.ssisauth.events;

import dev.keii.ssisauth.SSISAuth;
import net.kyori.adventure.text.Component;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit {
    @EventHandler
    public static void onPlayerQuit(PlayerQuitEvent e) {
        e.quitMessage(Component.text("§e" + SSISAuth.Users.get(e.getPlayer().getUniqueId().toString()).getDisplayName() + " left the server"));
    }
}
