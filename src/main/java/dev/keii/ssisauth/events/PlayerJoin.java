package dev.keii.ssisauth.events;

import dev.keii.ssisauth.chunks.ChunksDatabase;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static dev.keii.ssisauth.commands.CommandReloadUsers.updateUser;

import dev.keii.ssisauth.User;

public class PlayerJoin implements Listener {
    private static void setChunksNickname(User user, Player player)
    {
        try {
            Connection connection = ChunksDatabase.getConnection();
            Statement statement = connection.createStatement();

            String updateNicknameQuery = "UPDATE user SET nickname = '" + user.getDisplayName() + "' WHERE uuid = '" + player.getUniqueId() + "'";

            statement.execute(updateNicknameQuery);

            statement.close();
            connection.close();
        } catch (SQLException e) {
            Bukkit.getServer().broadcast(Component.text("Fatal Database Error: " + e.getMessage()).color(NamedTextColor.RED));
        }
    }

    @EventHandler
    public static void onPlayerJoin(PlayerJoinEvent e) {
        User user = updateUser(e.getPlayer());

        if(user != null) {
            setChunksNickname(user, e.getPlayer());
        }

        e.joinMessage(Component.text("§e" + user.getSchoolClass() + " " + user.getDisplayName() + " joined the game"));
        e.getPlayer().sendMessage(Component.text("Glöm inte att du kan använda /map för att claima mark.").color(NamedTextColor.YELLOW));
    }
}
