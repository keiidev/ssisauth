package dev.keii.ssisauth.commands;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import dev.keii.ssisauth.SSISAuth;
import dev.keii.ssisauth.User;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Set;

import static dev.keii.ssisauth.global.playerClass;
import static dev.keii.ssisauth.global.playerPermission;

public class CommandReloadUsers implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        SSISAuth.Users.clear();
        for(Player player : Bukkit.getServer().getOnlinePlayers()){
            SSISAuth.Users.put(player.getUniqueId().toString(), updateUser(player));
        }

        return true;
    }

    @Nullable
    public static User updateUser(Player player)
    {
        HttpRequest request;
        HttpResponse<String> response;
        String responseText;
        try {
            HttpClient client = HttpClient.newHttpClient();
            request = HttpRequest.newBuilder()
                    .uri(URI.create(SSISAuth.config.apiUrl + "/api/v1/checkuserjava?uuid=" + player.getUniqueId().toString()))
                    .build();

            response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());

            responseText = response.body();
        } catch(Exception error) {
            player.kick(Component.text().content("§cOj! Ser ut som att APIet har lite problem. Kontakta 22widi@stockholmscience.se. " + error.getMessage()).build());
            return null;
        }

        //Bukkit.getServer().broadcast(Component.text().content(String.valueOf(response.statusCode())).build());

        if(response.statusCode() == 400) {
            player.kick(Component.text().content(responseText).build());
            return null;
        } else if(response.statusCode() == 200) {
            String[] result = responseText.split(",");

            User user = new User(result[3], result[4], result[2], Integer.parseInt(result[5]) == 1, result[3] + result[4].charAt(0));

            //Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"nick " + e.getPlayer().getName() + " " + result[3] + result[4].charAt(0));
            if(user.getAdmin()) {
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player.getName() + " meta setprefix &6[" + user.getSchoolClass() + "]");
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player.getName() + " group add staff");
            } else {
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player.getName() + " meta setprefix &e[" + user.getSchoolClass() + "]");
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "lp user " + player.getName() + " group remove staff");
            }

            if(!user.getAdmin()) {
                player.playerListName(Component.text().content(ChatColor.YELLOW + user.getSchoolClass() + " " + ChatColor.WHITE + user.getDisplayName()).build());
            } else {
                player.playerListName(Component.text().content(ChatColor.GOLD + user.getSchoolClass() + " " + ChatColor.WHITE + user.getDisplayName() + " " + ChatColor.GOLD + "Staff").build());
            }
            //e.getPlayer().playerListName(Component.text().content(ChatColor.WHITE + result[3] + result[4].charAt(0)).build());

            playerClass.remove(user.getDisplayName());
            playerClass.put(user.getDisplayName(), user.getSchoolClass());

            playerPermission.remove(user.getDisplayName());
            playerPermission.put(user.getDisplayName(), user.getAdmin() ? 1 : 0);

            player.displayName(Component.text(user.getDisplayName()));
            PlayerProfile oldProfile = player.getPlayerProfile();
            Set<ProfileProperty> old = oldProfile.getProperties();
            var profile = Bukkit.createProfileExact(player.getUniqueId(), user.getDisplayName());
            profile.setProperties(old); // The players previous properties
            player.setPlayerProfile(profile);
            return user;
        }
        return null;
    }
}