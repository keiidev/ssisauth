package dev.keii.ssisauth;

import dev.keii.ssisauth.commands.CommandReloadUsers;
import dev.keii.ssisauth.commands.CommandWho;
import dev.keii.ssisauth.events.PlayerJoin;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class SSISAuth extends JavaPlugin {
    private static SSISAuth instance;
    public static Config config;

    // User uuid, User
    public static HashMap<String, User> Users = new HashMap<>();

    @Override
    public void onEnable() {
        instance = this;

        config = new Config();
        config.loadConfig();

        this.getCommand("who").setExecutor(new CommandWho());
        this.getCommand("reloadusers").setExecutor(new CommandReloadUsers());
        getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "SSISAuth Enabled!");
        getServer().dispatchCommand(getServer().getConsoleSender(), "reloadusers");
    }

    @Override
    public void onDisable() {
        getServer().getConsoleSender().sendMessage(ChatColor.RED + "SSISAuth Disabled");
    }

    public static SSISAuth getInstance()
    {
        return instance;
    }
}
