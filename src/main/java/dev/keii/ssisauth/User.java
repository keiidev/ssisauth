package dev.keii.ssisauth;

public class User {
    private String firstName;
    private String lastName;
    private String schoolClass;

    private Boolean isAdmin;

    private String displayName;

    public User(String firstName, String lastName, String schoolClass, Boolean isAdmin, String displayName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.schoolClass = schoolClass;
        this.isAdmin = isAdmin;
        this.displayName = displayName;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public String getDisplayName() {
        return displayName;
    }
}
